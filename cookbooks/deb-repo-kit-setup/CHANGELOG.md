# CHANGELOG for deb-repo-kit-setup

This file is used to list changes made in each version of deb-repo-kit-setup.

## 0.1.0:

* Initial release of deb-repo-kit-setup

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
