deb-repo-construction-kit
=========================

No point in building debian packages if you can't host them and use `apt` to install stuff. This repository contains all the necessary tools for setting up a secure debian repository.

Requirements
============
As usual you need `ruby`, `rubygems`, `bundler`. You can install all those on a Ubuntu or any other Debian based system with:

    $ sudo apt-get update
    $ sudo apt-get install rubygems
    $ sudo gem install bundler --no-ri --no-rdoc
    $ sudo bundle

Instructions
============
Once you have the required ruby tools. Just run

    $ ruby main.rb --help

A typical way to set up a debian repository pool is to run

    $ sudo ruby main.rb --repo-root-directory /var/www/debs --pool-name qa --force-yes

The above command will set up the proper directory structure for a debian repository which you can add to your `sources.list`. To
add the repository to your `sources.list` take note of the ip address of the box the repo is hosted on add add the following line
to your `/etc/apt/sources.list`

    deb [arch=amd64] http://$ip_address/$repo_root $pool_name main

You also need to import the GPG keys for secure apt which you can do by running the following command

    $ wget -qO- http://$ip_address/$repo_root/reposignkey_pub.gpg | sudo apt-key add -


