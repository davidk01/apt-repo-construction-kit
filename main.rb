require 'rubygems'
require 'erb'
require 'ostruct'
require 'trollop'

$opts = Trollop::options do
  opt :repo_root_directory, "The root directory for the repository, e.g. /var/www/milo. Pools go in this directory.",
    :required => true, :type => :string
  opt :pool_name, "You need to create at least one pool.",
    :required => true, :type => :string
  opt :force_yes, "Force yes to all questions.", :required => false, :type => :bool
end

if !(ENV["SUDO_COMMAND"] || ENV["USER"] == "root")
  raise StandardError, "Need to run with sudo or be root."
end

puts "Installing requirements: apt-utils, apache2, incron, gnupg, rng-tools."
`apt-get update`
ENV['DEBIAN_FRONTEND'] = 'noninteractive'
`apt-get -y -q install apt-utils apache2 incron gnupg rng-tools`

incron_allow_file = '/etc/incron.allow'
puts "Modifying #{incron_allow_file} to allow root to use it."
incron_allow_contents = File.read(incron_allow_file)
if incron_allow_contents =~ /root/
  puts "root is already allowed to use incron."
else
  puts "Adding root to #{incron_allow_file}."
  incron_allow_contents << "\nroot"
  open(incron_allow_file, 'w') {|f| f.puts incron_allow_contents}
end

# create root directory
if !File.directory?($opts[:repo_root_directory])
  puts "It doesn't look like #{$opts[:repo_root_directory]} exists. Attempting to create."
  puts `mkdir -p #{$opts[:repo_root_directory]}`
end

dists_directory = File.join($opts[:repo_root_directory], "dists")
# create pool directories
[
  File.join($opts[:repo_root_directory], "pool", $opts[:pool_name]),
  File.join(dists_directory, $opts[:pool_name], "main", "binary-amd64")
].each do |dir|
  if !File.directory?(dir)
    puts "It doesn't look like #{dir} exists. Attempting to create."
    puts `mkdir -p #{dir}`
  end
end

# will be used for passing bindings to templates
namespace = OpenStruct.new(
  :pool_name => $opts[:pool_name], 
  :repo_root_directory => $opts[:repo_root_directory],
  :pool_directory => File.join($opts[:repo_root_directory], "pool", $opts[:pool_name]))

content_generator = lambda do |template_content, outputfile|
  puts "Generating content for #{outputfile}."
  template = ERB.new(template_content, 0, '>')
  content = template.result(namespace.instance_eval { binding })
  open(outputfile, 'w') {|f| f.puts content}
end

# generate apt-ftparchive-$pool_name.conf and place at the pool directory
pool_config_template = <<-EOF
Dir {
  ArchiveDir "<%= repo_root_directory %>";
  CacheDir "<%= repo_root_directory %>";
};

Default {
  Packages::Compress ". gzip bzip2";
  Sources::Compress ". gzip bzip2";
  Contents::Compress ". gzip bzip2";
};

// Includes the main section. You can structure the directory tree under
// ./pool/main any way you like, apt-ftparchive will take any deb (and
// source package) it can find. This creates a Packages a Sources and a
// Contents file for these in the main section of the sid release
BinDirectory "pool/<%= pool_name %>/" {
  Packages "dists/<%= pool_name %>/main/binary-amd64/Packages";
  Contents "dists/<%= pool_name %>/Contents-amd64";
  BinCacheDB "cache";
  FileMode "0644";
}

// By default all Packages should have the extension ".deb"
Default {
  Packages {
    Extensions ".deb";
  };
};
EOF
content_generator.call(pool_config_template, File.join(namespace.pool_directory, "apt-ftparchive-#{$opts[:pool_name]}.conf"))

# do the same for release.conf
ftp_archive_config_template = <<-EOF
APT::FTPArchive::Release::Codename "<%= pool_name %>";
APT::FTPArchive::Release::Architectures "amd64";
APT::FTPArchive::Release::Components "main";
EOF
content_generator.call(ftp_archive_config_template, File.join(namespace.pool_directory, "release-#{$opts[:pool_name]}.conf"))

# ask the user if they want to generate their own keys or use the ones
# that comes with this repo
print "Do you want to generate your own GPG keys. If you say no then existing keys will be used? (y/n) "
if $opts[:force_yes] || STDIN.gets.strip =~ /y/
  puts "Generating keys."
  puts `gpg --list-keys`
  `sudo rngd -r /dev/urandom -o /dev/random`
  puts `gpg --gen-key --armor --batch gpgkeybatch`
  puts "Keys generated. Getting key id."
else
  puts "Using existing keys."
end

# import signing keys for root and make the public key available at repo root
puts "Importing GPG keys to root keyring."
puts `sudo -u root -H -s gpg --import keys/reposignkey_pub.gpg`
puts `sudo -u root -H -s gpg --allow-secret-key-import --import keys/reposignkey_sec.gpg`
puts "Copying public key to repo root: #{$opts[:repo_root_directory]}."
`cp keys/reposignkey_pub.gpg #{$opts[:repo_root_directory]}`

# we need the key id otherwise signing won't work
key_id = nil
`sudo -u root -H -s gpg --list-keys`.split("\n")[2..-1].each_slice(2) do |key_data, comment|
  if comment =~ /Do not change this./
    key_id = key_data.match(/\/(.+?) /)[1]; break
  end
end
if key_id.nil?
  raise StandardError, "Could not find GPG key ID."
end

# generate pool rebuild script
rebuild_template = <<-EOF
#!/bin/sh

REPO=<%= repo_root_directory %>

LIB=<%= pool_directory %>

apt-ftparchive generate $LIB/apt-ftparchive-<%= pool_name %>.conf
apt-ftparchive clean $LIB/apt-ftparchive-<%= pool_name %>.conf
apt-ftparchive -c $LIB/release-<%= pool_name %>.conf release $REPO/dists/<%= pool_name %> > $REPO/dists/<%= pool_name %>/Release
rm -rf $REPO/dists/<%= pool_name %>/Release.gpg
gpg -u #{key_id} -bao $REPO/dists/<%= pool_name %>/Release.gpg $REPO/dists/<%= pool_name %>/Release
EOF
script_filename = File.join(namespace.pool_directory, "rebuild-repo-#{$opts[:pool_name]}.sh")
content_generator.call(rebuild_template, script_filename)
`chmod +x #{script_filename}`

# generate incron.d config for watching for file changes in the given pool
incron_template = "<%= pool_directory %> IN_CREATE,IN_NO_LOOP touch <%= pool_directory %>/needs_rebuild"
content_generator.call(incron_template, File.join("/etc/incron.d/", "rebuild-watcher-#{$opts[:pool_name]}"))

# generate cron.d script for rebuild job
cron_content = "*/1 * * * * root <%= pool_directory %>/cron-rebuilder-#{$opts[:pool_name]}"
content_generator.call(cron_content, File.join("/etc/cron.d/", "rebuild-check-#{$opts[:pool_name]}"))

# generate ruby script for rebuilding the cache
cache_rebuilder_template = <<-EOF
#!/usr/bin/env ruby
require 'fileutils'

if File.exists?("<%= pool_directory %>/needs_rebuild") && !File.exists?("<%= pool_directory %>/rebuilding")
  FileUtils.touch "<%= pool_directory %>/rebuilding"
  sleep 1
  FileUtils.rm "<%= pool_directory %>/needs_rebuild"
  puts `<%= pool_directory %>/rebuild-repo-<%= pool_name %>.sh`
  FileUtils.rm "<%= pool_directory %>/rebuilding"
else
  puts "Either there is no need to rebuild or a rebuild is already in progress."
end
EOF
script_filename = File.join(namespace.pool_directory, "cron-rebuilder-#{$opts[:pool_name]}")
content_generator.call(cache_rebuilder_template, script_filename)
`chmod +x #{script_filename}`

puts "Starting incron."
puts `service incron start`

# Done!
puts "You can now generate the repository by running the generated rebuild script found at " +
 "#{$opts[:repo_root_directory]}/pool/#{$opts[:pool_name]}/rebuild-repo-#{$opts[:pool_name]}.sh as root."
